#!/bin/sh
killall xcape
killall xmodmap

# Basics
xset r rate 200
setxkbmap de -option caps:escape #,altwin:swap_lalt_lwin

### THUMBS
xmodmap -e "keycode 133 = Escape"
xmodmap -e "keycode any = Super_L"
xcape -e "#133=Escape"

### LEFT HAND
# mod-tap super a
xmodmap -e "keycode 38 = Super_L"
xmodmap -e "keycode any = a"
xcape -e "Super_L=a"
# mod-tap alt s
xmodmap -e "keycode 39 = Alt_L"
xmodmap -e "keycode any = s"
xcape -e "Alt_L=s"
# mod-tap control d
xmodmap -e "keycode 40 = Control_L"
xmodmap -e "keycode any = d"
xcape -e "Control_L=d"
# mod-tap shift f
xmodmap -e "keycode 41 = Shift_L"
xmodmap -e "keycode any = f"
xcape -e "Shift_L=f"

### RIGHT HAND
# mod-tap shift j
xmodmap -e "keycode 44 = Shift_R"
xmodmap -e "keycode any = j"
xcape -e "Shift_R=j"
# mod-tap control k
xmodmap -e "keycode 45 = Control_R"
xmodmap -e "keycode any = k"
xcape -e "Control_R=k"
# mod-tap alt l
xmodmap -e "keycode 46 = Alt_R"
xmodmap -e "keycode any = l"
xcape -e "Alt_R=l"
# mod-tap super '
#xmodmap -e "keycode 47 = Super_R"
#xmodmap -e "keycode any = '"
#xcape -e "Super_R='"
