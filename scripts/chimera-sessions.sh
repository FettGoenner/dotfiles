#!/usr/bin/env bash

# CMDs
uptime="`uptime -p | sed -e 's/up //g'`"
DIR=$(dirname "$0")
THEME="mytheme"

desktopw="  DesktopW"
desktopx="  DesktopX"
steam="󰓓  Steam"
steamp="  Steam"
opengpui="  OpenGamepadUI"
power="󰤆  Power"

# rarch="  RetroArch"
# kodi="󰌔  Kodi"
# firefox="󰈹  Firefox"
# brave="󰊯  Brave"


# Rofi CMD
rofi_cmd() {
    rofi -dmenu \
        -mesg "  $uptime" \
        -theme ${DIR}/${THEME}.rasi
}

# Pass variables to rofi dmenu
run_rofi() {
    echo -e \
        "$desktopw\n$desktopx\n$steam\n$steamp\n$opengpui\n$power" | rofi_cmd
}

# Actions
chosen="$(run_rofi)"
case ${chosen} in
    $return)
        ;;
    $desktopw)
        chimera-session desktop
        ;;
    $desktopx)
        chimera-session desktop-xorg
        ;;
    $steam)
        chimera-session steam
        ;;
    $steamp)
        chimera-session steam-plus
        ;;
    $opengpui)
        chimera-session opengamepadui
        ;;
    $power)
        bash $DIR/mypower.sh
        ;;
esac
