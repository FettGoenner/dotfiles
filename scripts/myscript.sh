#!/usr/bin/env bash
# Current Theme
DIR=$(dirname "$0")
theme="mytheme"

# CMDs
lastlogin="`last $USER | head -n1 | tr -s ' ' | cut -d' ' -f5,6,7`"
uptime="`uptime -p | sed -e 's/up //g'`"

steam="󰓓  Steam"
rarch="  RetroArch"
kodi="󰌔  Kodi"
firefox="󰈹  Firefox"
brave="󰊯  Brave"
power="  Power"

# args: windowname, progname, desktop
runswitch() {
    if [[ $(wmctrl -l | grep $1) ]]; then
        wmctrl -a $1 # show window
    else
        wmctrl -s $3 # go to that desktop
        $2           # run the program
        #wmctrl -R $1 # move window to current desktop
    fi

}

# Rofi CMD
rofi_cmd() {
    rofi -dmenu \
        -mesg "  $uptime" \
        -theme ${DIR}/${theme}.rasi
}

# Pass variables to rofi dmenu
run_rofi() {
    echo -e "$steam\n$rarch\n$kodi\n$firefox\n$brave\n$power" | rofi_cmd
}

# Actions
chosen="$(run_rofi)"
case ${chosen} in
    $return)
        ;;
    $steam)
        runswitch Steam steam 5
        ;;
    $rarch)
        runswitch RetroArch retroarch 6
        ;;
    $kodi)
        runswitch Kodi kodi 7
        ;;
    $firefox)
        runswitch Firefox firefox 8
        ;;
    $brave)
        runswitch Brave brave 8
        ;;
    $power)
        bash $HOME/.config/rofi/mypower.sh
        ;;
esac
