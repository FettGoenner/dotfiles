#!/usr/bin/env bash

## Author : Aditya Shakya (adi1090x)
## Github : @adi1090x

dir="$HOME/dotfiles/scripts"
theme='style-1' # 1,4,5

## Run
rofi \
    -show drun \
    -theme ${dir}/${theme}.rasi
