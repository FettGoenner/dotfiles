#!/usr/bin/env bash
# Current Theme
dir="$HOME/.config/rofi"
theme="mytheme"

# CMDs
lastlogin="`last $USER | head -n1 | tr -s ' ' | cut -d' ' -f5,6,7`"
uptime="`uptime -p | sed -e 's/up //g'`"

# Option names shown in rofi
done="done..."
steam="󰓓  Steam"
rarch="  Retroarch"
kodi="󰌔  Kodi"
firefox="󰈹  Firefox"
brave="󰊯  Brave"
power="  Powermenu"

#shutdown=''
#reboot=''
#lock=''
#hibernate=''
#suspend=''
#logout=''
#yes=''
#no=''

# args: winname,progname
runswitch() {
    if [[ $(wmctrl -l | grep $1) ]]; then
        echo "Already running. Switching to $1."
        wmctrl -a $1 # winname
    else
        echo "Not running. Launching $1."
        $2 # progname
    fi
}

# Rofi CMD
rofi_cmd() {
    rofi -show window \
        -mesg "  $uptime" \
        -theme ${dir}/${theme}.rasi
}

# Pass variables to rofi dmenu
run_rofi() {
    echo -e "$steam\n$rarch\n$kodi\n$firefox\n$brave\n$power" | rofi_cmd
}

rofi_cmd
# Actions
#chosen="$(run_rofi)"
case ${chosen} in
    $return)
        ;;
    $steam)
        runswitch Steam steam
        ;;
    $rarch)
        runswitch RetroArch retroarch
        ;;
    $kodi)
        runswitch Kodi kodi
        ;;
    $firefox)
        runswitch Firefox firefox
        ;;
    $brave)
        runswitch Brave brave
        ;;
    $power)
        bash $HOME/.config/rofi/mypower.sh
        ;;
esac
