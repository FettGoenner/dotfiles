#!/bin/sh

stow alacritty
stow git
stow nvim
stow picom
stow qtile
stow rofi
stow sxhkd
stow vifm
stow xinit
stow zsh

# ZSH fast syntax highlighting
#git clone https://github.com/zdharma-continuum/fast-syntax-highlighting ~/.config/to/fsh

# ZSH Zinit
bash -c "$(curl --fail --show-error --silent --location https://raw.githubusercontent.com/zdharma-continuum/zinit/HEAD/scripts/install.sh)"
source $ZDOTDIR/.zshrc
zinit self-update

# NEOVIM Plug
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
