if status is-interactive
    # Commands to run in interactive sessions can go here
    set -U fish_greeting
    fish_vi_key_bindings
    set fish_cursor_default block
    set fish_cursor_insert line
    set fish_cursor_replace underscore
    set fish_cursor_replace_one underscore
    set fish_cursor_external block
    set fish_cursor_visual block
end

# Modify mode prompt and its colors
function fish_mode_prompt
    switch $fish_bind_mode
    case default
        set_color --bold brwhite #grey
        echo '[N] '
    case replace_one
        set_color --bold grey
        echo '[R] '
    case replace
        set_color --bold brcyan
        echo '[R] '
    case insert
        set_color --bold brblue
        echo '[I] '
    case visual
        set_color --bold yellow
        echo '[V] '
    case '*'
        set_color --bold red
        echo '[?] '
    end
    set_color normal
end
