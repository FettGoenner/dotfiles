export ZDOTDIR="$HOME/.config/zsh"

typeset -U path PATH
path=($HOME/blender/blender-3.3.0 $path)
path=($HOME/blender/blender-3.4.1 $path)
path=($HOME/.cargo/bin $path)
export PATH
#export PYTHONPATH="$HOME/blender/venv/lib/python3.10/site-packages/"
