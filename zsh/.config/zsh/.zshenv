export ZDOTDIR="$HOME/.config/zsh"

typeset -U path PATH
path=($HOME/blender/blender-3.3.0 $path)
path=($HOME/.cargo/bin $path)
export PATH
