# My Dotfiles with beautiful gruvbox-theme

These are all my configuration files for my GNU/Linux desktop.
In contrast to traditional desktop environments, my desktop
is a keyboard-driven, easy on the hardware, highly customized experience
which will make you feel **in control**.

---

The file structure of this repo is made for usage with GNU stow,
therefore being confusing. See [Installation](#2) for more info.

## Now we talkin'
1. [Preview](#1)
2. [Installation](#2)
3. [File Overview](#3)

<h2 id="1">
Preview
</h2>
![Preview](wallpapers/preview.png)

Wallpaper by [Calder Moore](https://www.artstation.com/refriedspinach)


<h2 id="2">
Installation
</h2>

1. Move old config files that sit in the way of mine.
2. Execute the following commands in your Terminal:
```console
$ git clone https://gitlab.com/FettGoenner/dotfiles
$ cd dotfiles
$ ./install.sh
```

#### How does the installation work?
The repository is downloaded to your local machine via `git clone`. \
You move into the downloaded repo via `cd`. \
The script [install.sh](install.sh) creates simlinks to
all the config files in the correct place. \

With this method, a symlink to the file
`~/dotfiles/bash/.bashrc` is created at
`~/.bashrc` for example.

And a symlink to `~/dotfiles/nvim/.config/nvim/init.vim` is
created at `~/.config/nvim/init.vim`.

If all that is confusing, read more about GNU Stow
[here](https://www.gnu.org/software/stow/).
