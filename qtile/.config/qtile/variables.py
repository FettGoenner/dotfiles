from gruvbox import gruv
import psutil
# My custom variables

SPACE_WIDTH = 2
BORDER_WIDTH = 4
BORDER_COLOR_FOCUS = gruv["yellow"]
BORDER_COLOR = gruv["gray"]
GAP_WIDTH = 0

### BATTERY
battery = psutil.sensors_battery()
LAPTOP = True
if battery is None:
    LAPTOP = False
