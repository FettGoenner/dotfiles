# FETT GOENNER'S QTILE KEYBOARD AND GROUP CONFIG
#
# XXX IMPORTANT:
# This is only used for controlling windows with the
# MonadTall (master/stack) layout.
# Spawning windows and launching programs (except Dropdown Terminal)
# is done via sxhkd, NOT qtile.

from libqtile.config import Key, Group, Match, ScratchPad, DropDown, Drag, Click, Rule
from libqtile.lazy import lazy

import os

HOME = os.path.expanduser("~/")
DROP_SIZE   = 0.6 # %
DROP_X      = (1 - DROP_SIZE)/2
DROP_Y      = (1 - DROP_SIZE)/2

names = [
    [],                                             # group 1
    ["zoom", "mtga.exe"],                           # group 2
    [],                                             # group 3
    ["Steam"],                                      # group 4
    ["TelegramDesktop", "Signal", "UXTerm", "St",   # group 5
     "Chromium"]]

matches = [[Match(name) for name in names[i]] for i in range(len(names))]

groups = [
    Group("1", matches=matches[0]),
    Group("2", matches=matches[1]),
    Group("3", matches=matches[2]),
    Group("4", matches=matches[3]),
    Group("5", matches=matches[4]),
    ScratchPad("s", [
        DropDown("term", "alacritty",
                 width=DROP_SIZE, height=DROP_SIZE, x=DROP_X, y=DROP_Y,
                 opacity=1,
                 ),
        DropDown("vifm",
                 ["alacritty", "-e", HOME + "/.config/vifm/scripts/vifmrun"],
                 width=DROP_SIZE, height=DROP_SIZE, x=DROP_X, y=DROP_Y,
                 opacity=1,
                 )
    ]),
]
