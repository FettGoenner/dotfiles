from libqtile import layout
from libqtile.config import Match

from variables import *
from gruvbox import gruv

layouts = [
    layout.MonadTall(
        border_focus    =   BORDER_COLOR_FOCUS,
        border_normal   =   BORDER_COLOR,
        border_width    =   BORDER_WIDTH,
        margin          =   GAP_WIDTH,
        new_client_position =   'bottom',
    ),
    layout.Max(),
    # layout.Columns(
    #     border_focus    =   BORDER_COLOR_FOCUS,
    #     border_normal   =   BORDER_COLOR,
    #     border_width    =   BORDER_WIDTH,
    #     margin          =   GAP_WIDTH,
    # ),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

floating_layout = layout.Floating(float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class='zoom'), # ADDED
        Match(wm_class='out'), # ADDED
        Match(wm_class='output'), # ADDED
        Match(wm_class='QjackCtl'),
        Match(wm_class='confirmreset'), # gitk
        Match(wm_class='makebranch'),  # gitk
        Match(wm_class='maketag'),  # gitk
        Match(wm_class='ssh-askpass'),  # ssh-askpass
        Match(title='branchdialog'),  # gitk
        Match(title='pinentry'),  # GPG key password entry
    ],
    border_focus    =   gruv["yellow"],
    border_normal   =   gruv["bg"],
    border_width    =   BORDER_WIDTH,
)
