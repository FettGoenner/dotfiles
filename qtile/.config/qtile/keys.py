# FETT GOENNER'S QTILE KEYBOARD CONFIG
#
# Also binding the mouse to act on floating stuff.
#
# XXX IMPORTANT:
# This is only used for controlling windows with the
# MonadTall (master/stack) layout.
# Spawning windows and launching programs (except Dropdown Terminal)
# is done via sxhkd, NOT qtile.

from libqtile.config import Key, Drag, Click
from libqtile.command import lazy as lazycmd
from libqtile.lazy import lazy

from groups import groups

mod = "mod4"

keys = [
    # Switch between windows
    Key([mod], "j", lazy.group.next_window()),
    Key([mod], "k", lazy.group.prev_window()),
    # Move windows
    Key([mod, "control"], "j",
        lazy.layout.shuffle_down().when(when_floating=False)),
    Key([mod, "control"], "k",
        lazy.layout.shuffle_up().when(when_floating=False)),
    # Grow/shring windows
    Key([mod], "l", lazy.layout.grow(),
        desc="Grow stack"),
    Key([mod], "h", lazy.layout.shrink(),
        desc="Shrink Stack"),
    # Toggle/cycle different layouts as defined below
    Key([mod], "return", lazy.next_layout()),
    #Key([mod, "shift"], "space", lazy.prev_layout()),
    #Key([mod], "f", lazy.window.toggle_fullscreen()),
    # Bar
    Key([mod], "b", lazy.hide_show_bar("top")),
    ### Floating controls with MOD+...
    # Key([mod, "control"], "h",
    #     lazy.window.move_floating(-100,    0).when(when_floating=True)),
    # Key([mod, "control"], "l",
    #     lazy.window.move_floating( 100,    0).when(when_floating=True)),
    # Key([mod, "control"], "k",
    #     lazy.window.move_floating(   0, -100).when(when_floating=True)),
    # Key([mod, "control"], "j",
    #     lazy.window.move_floating(   0,  100).when(when_floating=True)),
    Key([mod, "shift"], "return", lazy.window.toggle_floating()),
    # Restarting and killing
    Key([mod], "q", lazy.window.kill()),
    Key([mod], "r", lazy.restart()),
    #Key([mod], "x", lazy.shutdown()),
    # Dropdown
    Key([mod], "t", lazy.group["s"].dropdown_toggle("term")),
    Key([mod], "v", lazy.group["s"].dropdown_toggle("vifm")),
]

groupkeys = "asdfg"
for i in range(len(groups) - 1): # ignoriere ScratchPad
    g = groups[i]
    k = groupkeys[i]
    keys.extend([
        Key([mod], k, lazy.group[g.name].toscreen(toggle=False)),
        Key([mod, "control"], k, lazy.window.togroup(g.name, switch_group=False)),
    ])

mouse = [
    # Drag floating windows
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod, "shift"], "Button3", lazy.window.toggle_floating()),
    # Move windows
    Click([mod, "shift"], "Button4", lazy.layout.shuffle_up()),
    Click([mod, "shift"], "Button5", lazy.layout.shuffle_down()),
    # Grow/shrink stack
    Click([mod], "Button4", lazy.layout.shrink()),
    Click([mod], "Button5", lazy.layout.grow()),
]
