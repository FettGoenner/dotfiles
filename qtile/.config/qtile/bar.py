# FETT GOENNER'S QTILE SCREEN AND BAR CONFIG

from libqtile import bar, widget
from gruvbox import gruv
from battery import bat_widgets
from variables import SPACE_WIDTH, LAPTOP

widget_defaults = dict(
    font='Inconsolata',
    fontsize    =   16,
    padding     =   8,
    background  =   gruv["bg"],
    foreground  =   gruv["bg"],
)
extension_defaults = widget_defaults.copy()

front_widgets = [
    # Groups (Workspaces) Block
    widget.GroupBox(
        this_current_screen_border  =   gruv["yellow"],
        block_highlight_text_color  =   gruv["bg"],
        active          =   gruv["fg"],
        inactive        =   gruv["gray"],
        other_screen_border = gruv["fg"],
        highlight_method=   "block",
        urgent_border   =   gruv["red"],

        padding_x       =   12,
        padding_y       =   4,
        margin_x        =   0,
        margin_y        =   3,

        spacing         =   0,
        borderwidth     =   0,
        disable_drag    =   True,
        rounded         =   False,
    ),
    # Window Name Block
    widget.WindowName(
        #background  =   gruv["bg"],
        foreground  =   gruv["fg"],
    ),
    # Space
    widget.Spacer(SPACE_WIDTH),
    # System Tray Block
    widget.Systray(
        background      =   gruv["purple"],
    ),
    widget.Spacer(8, background=gruv["purple"]),
    # Space
    widget.Spacer(SPACE_WIDTH),
    # Volume Block
    widget.Volume(
        emoji       = True,
        emoji_list  = ['󰝟', '󰕿', '󰖀', '󰕾'],
        background  = gruv["blue"],
        foreground  = gruv["fg"],
        mute_command    =   "amixer sset Master toggle",
    ),
    widget.Volume(
        background      =   gruv["fg"],
        mute_command    =   "amixer sset Master toggle",
        update_interval =   0.05,
    ),
    # widget.PulseVolume(
    #     background      =   gruv["fg"],
    #     volume_app      =   "pavucontrol",
    #     mute_command    =   "amixer sset Master toggle",
    #     update_interval =   0.05,
    # ),
    # Space
    widget.Spacer(SPACE_WIDTH),
    # Update Block
    widget.TextBox(' ',
        background      =   gruv["aqua"],
        foreground      =   gruv["fg"],
    ),
    widget.CheckUpdates(
        background      =   gruv["fg"],
        update_interval =   60,
        custom_command  =   "checkupdates",
        no_update_string    =   "0",
        display_format      =   '{updates}',
        colour_have_updates =   gruv["bg"],
        colour_no_updates   =   gruv["bg"],
    ),
]

back_widgets = [
    # Space
    widget.Spacer(SPACE_WIDTH),
    # Date Block
    widget.TextBox(' ',
        background      =   gruv["yellow"],
        foreground      =   gruv["fg"],
    ),
    widget.Clock(
        background      =   gruv["fg"],
        format=' %A %d.%m.%Y ',
    ),
    # Space
    widget.Spacer(SPACE_WIDTH),
    # Time Block
    widget.TextBox(' ',
        background      =   gruv["orange"],
        foreground      =   gruv["fg"],
    ),
    widget.Clock(
        format=' %H:%M ',
        background      =   gruv["fg"],
    ),
    # Space
    #widget.Spacer(SPACE_WIDTH),
]

widgets = front_widgets
if LAPTOP:
    widgets += bat_widgets
widgets += back_widgets

bar1 = bar.Bar(
    widgets,
    24,
    margin = 0,
)
