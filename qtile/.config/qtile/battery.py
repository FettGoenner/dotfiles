import psutil
from time import sleep
from libqtile import widget
from variables import SPACE_WIDTH
from gruvbox import gruv


ICON_SPACE_WIDTH = 3*SPACE_WIDTH

bat_widgets = [
    widget.Spacer(SPACE_WIDTH),
    widget.Spacer(ICON_SPACE_WIDTH, background = gruv["green"],),
    widget.BatteryIcon(
        theme_path = "~/.config/qtile/battery_icons/",
        scale = 1.5,
        background = gruv["green"],
    ),
    widget.Spacer(ICON_SPACE_WIDTH, background = gruv["green"],),
    widget.Battery(
        format          = "{percent:2.0%}",
        full_char       = "{percent:2.0%}",
        background      = gruv["fg"],
        low_background  = gruv["red"],
        low_foreground  = gruv["bg"],
        low_percentage  = 0.25,
        notify_below    = 0.25,
    ),
]
