" No background color
autocmd vimenter * hi Normal guibg=NONE
" autoremove trailing whitespace
autocmd BufWritePre *.py :%s/\s\+$//e

:command P :!python %

filetype plugin indent on
set notimeout           " No timeout for leader key
set nocompatible        " Polyglot

" Visuals
syntax enable           " Enables syntax highlighing
set termguicolors       " 24bit RGB colors
set encoding=utf-8      " Encoding
set fileencoding=utf-8  " The encoding written to file
set number              " line numbers
set relativenumber
set cursorline          " Highlight current line
"set showtabline =2      " Tabline
set nowrap
set colorcolumn=80

" Tabs and Spaces
set tabstop     =4      " Width of tab character
set softtabstop =4      " fine tunes the amount of white space to be added ?
set shiftwidth  =4      " Number of Spaces inserted at indentation
set expandtab           " Appropriate Number of Spaces to mimic/replace Tabs
set smarttab            " Smarter Tabbing

" Other Stuff
set clipboard=unnamedplus   " Clipboard with everything
set smartindent         " Smarter Indentation
set splitbelow          " new hsplit below current window
set splitright          " new vsplit right of current window
set nobackup            " coc recommended
set nowritebackup       " coc recommended
set formatoptions-=cro  " Stop newline continuation of comments
"set autochdir           " Always change directory to the one of the current file
"set iskeyword+=-        " Treat dash separated words as a word text object
