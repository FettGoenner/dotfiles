""" SETTINGS
source $HOME/.config/nvim/keys.vim
source $HOME/.config/nvim/general.vim
""" PLUGINS
call plug#begin('~/.config/nvim/plug')
Plug 'morhetz/gruvbox'                          " Best Colorscheme EVAAAAR
Plug 'scrooloose/nerdtree'                      " NERDTree
Plug 'vim-airline/vim-airline'                  " Airline
Plug 'vim-airline/vim-airline-themes'           " Airline Themes
Plug 'justinmk/vim-sneak'                       " Sneak
Plug 'neoclide/coc.nvim', {'branch': 'release'} " Auto Completion
Plug 'lilydjwg/colorizer'                       " Colors
Plug 'tpope/vim-surround'                       " Surround
Plug 'tpope/vim-commentary'                     " Comments
Plug 'jiangmiao/auto-pairs'                     " Auto Pairing
Plug 'lervag/vimtex'                            " LaTeX
Plug 'tpope/vim-fugitive'
Plug 'prabirshrestha/vim-lsp'
Plug 'daeyun/vim-matlab'
Plug 'nvim-lua/plenary.nvim'
Plug 'rust-lang/rust.vim'
Plug 'nvim-telescope/telescope.nvim', { 'tag': '0.1.4' }
Plug 'ThePrimeagen/harpoon'
"Plug 'sheerun/vim-polyglot'                     " Language Packs
"Plug 'octol/vim-cpp-enhanced-highlight'         " Better C++ syntax highlight
call plug#end()

""" Plugin Settings
colorscheme gruvbox
nnoremap <leader>n  :NERDTreeToggle %:h<CR>
nnoremap <leader>ff :Telescope find_files<CR>

map f <Plug>Sneak_s
map F <Plug>Sneak_S
nnoremap <leader>a :lua require("harpoon.mark").add_file()<CR>
nnoremap <leader>h :lua require("harpoon.ui").toggle_quick_menu()<CR>
nnoremap <leader>j :lua require("harpoon.ui").nav_next()<CR>
nnoremap <leader>k :lua require("harpoon.ui").nav_prev()<CR>


let NERDTreeQuitOnOpen=1

let R_external_term = 'alacritty --title=Nvim-R -e'

let g:mkdp_browser = 'firefox'

let g:vimtex_view_method = 'zathura'
