" Leaders
let mapleader = " "
let maplocalleader = ","

" Better paragraph navigation
nnoremap <C-k> <C-u>
nnoremap <C-j> <C-d>

" Quotes
nnoremap "iw ciw"<C-r>""
nnoremap 'iw ciw'<C-r>"'
" Buffers
" map <leader>j :bnext<cr>
" map <leader>k :bprevious<cr>
" map <leader>x :bdelete<cr>
